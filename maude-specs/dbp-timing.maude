***(

    This file is part of DBP, a real-time, probabilistic rewriting logic 
    specification of distance-bounding protocols, facilitating statistical
    model checking of various guessing and timing attacks and 
    countermeasures.

    Copyright (C) 2017-2018 Musab A. Alturki, musab.alturki@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
)

mod PRED is
    pr NAT .

    op p : NzNat -> Nat .
    eq p(s(N:Nat)) = N:Nat .
endm

mod BLIST is
  pr NAT .

  sort Bit NoBit MaybeBit .
  subsort Bit NoBit < MaybeBit .

  ---- bN is just noise
  op bN : -> NoBit [ctor] .

  op b : Nat -> [Bit] .
  mb b(0) : Bit .
  mb b(1) : Bit .

  op neg : Bit -> Bit .
  eq neg(b(0)) = b(1) .
  eq neg(b(1)) = b(0) .

  --- Useful predicate
  op notNoise : MaybeBit -> Bool .
  eq notNoise(bN) = false .
  eq notNoise(B:Bit) = true [owise] .

  sort BList .
  subsort Bit < BList .

  op nilBL : -> BList .
  op __ : BList BList -> BList [assoc id: nilBL] .
 
  var B : Bit . var L : BList .

  op head : BList -> [Bit] .
  eq head(B L) = B .
    
endm

mod RANDBLIST is
    pr BLIST .
    pr SAMPLER .

    var N : Nat .

    op genRandBList : Nat -> [BList] .
    eq genRandBList(0) = nilBL .
    eq genRandBList(s(N)) = 
	b( if sampleBerWithP(0.5) then 0 else 1 fi ) 
	genRandBList(N) . 
endm

mod ATTRIBUTES is
    pr PRED .
    pr RANDBLIST . 
    pr APMAUDE .

    --- states of an actor
    sort Status .
    ops pending initialized ready       : -> Status . 
    ops sending sent receiving received : -> Status . 
    ops completed aborted               : -> Status .

    --- Attributes
    op status:_      : Status    -> Attribute [format (m o d)] .  
    op round:_       : Nat       -> Attribute [format (m o d)] .  
    op list0:_       : BList   	 -> Attribute [format (m o d)] .  
    op list1:_       : BList  	 -> Attribute [format (m o d)] .  
    op challenge:_   : Bit       -> Attribute [format (m o d)] .
    op response:_    : MaybeBit  -> Attribute [format (m o d)] .
    op mbit-cnt:_    : Nat       -> Attribute [format (m o d)] .
    op atbound-cnt:_ : Nat       -> Attribute [format (m o d)] .
    op mtbound-cnt:_ : Nat       -> Attribute [format (m o d)] .
    op mtime-sent:_  : Float     -> Attribute [format (m o d)] .  
    op atime-sent:_  : Float     -> Attribute [format (m o d)] .  
    op mtime-recv:_  : Float     -> Attribute [format (m o d)] .  
    op atime-recv:_  : Float     -> Attribute [format (m o d)] .  
endm

mod CONFIG is
  ex ATTRIBUTES .
  
    --- Additional configuration fields
    op rounds : Nat   -> Config [format (nb! o)] .
    op h      : Float -> Config [format (nb! o)] .
	
    --- Object names
    op v : -> ActorName .   --- verifier
    op p : -> ActorName .   --- prover

	--- Message Contents
    ops initialize forwardHash beginRound recordTime sendChallenge : -> Content . 
    ops recordHash : BList BList -> Content .
    op  response   : MaybeBit -> Content .
    op  challenge  : Bit -> Content .
endm

mod PARAMS is
    pr FLOAT .
    pr NAT .
    
    ---- Global parameters
    ---- Protocol 
    op ROUNDS    : -> [Nat]   .  ---- The number of rounds n (length of a bit sequence)
    op MAXRTT    : -> [Float] .  ---- The maximum RTT allowed (time upper bound)
    op NOISE     : -> [Float] .  ---- The noise bias (probability of destroying a bit)

    ---- Verifier 
    op  VDCLK                   : -> [Bool]  .  ---- Is the verifier's clock discrete?
    ops sampleX sampleY sampleZ : -> [Float] .  ---- Verifier action delays X, Y and Z 

    ---- Prover 
    op PTYPE    : -> [Nat]   .  ---- Prover's behavior: honest or guessing 
    op sampleRD : -> [Float] .  ---- H: the prover's time differential from the bound 
    op GAHEAD   : -> [Bool]  .  ---- Is the prover guessing ahead?
    op gATD      : Float -> [Float] .  ---- Guess-ahead time differential
    ---op GATD     : -> [Float] .  ---- The guessing-ahead time differential 

    ---- Acceptance thresholds
    op MIN-MBR   : -> [Float] .    ---- Acceptance threshold based on correctness of bits
    op MIN-MTR   : -> [Float] .    ---- .. .. .. based on measured time
    op MIN-ATR   : -> [Float] .    ---- .. .. .. based on actual time

    --- Computed quantities
    op xDelay    : Float -> [Float] .  ---- (One-way) Trasmission delay
    op VDCDELAY  : -> [Float] .  ---- Verfier's discrete clock delay

endm

mod DBP-BEHAVIOR is
  pr CONFIG .
  pr PARAMS .
  pr SAMPLER .
  
  vars xR xR' : NzNat . 
  vars cR N iG N' N'' iR C : Nat . 
  vars L0 L1 L0' L1' : BList .
  vars B B0 B1 bR bR' cB : Bit .
  vars cP cP' : MaybeBit .
  var  AS : AttributeSet . 
  vars tG fB fH tD tX tS tS' tR tR' tRTT : Float .
  vars SL SL' : ScheduleList . 
  var ST : Status .
  vars mBit? atBound? mtBound? dB : Bool . 
   
  ***** Phase I
  --- Generate random lists and initialize the round counter
  rl [Initialize] : 
    rounds(xR)
    <name: v |
	       status: pending,
	       round: cR,
	       list0: L0,
	       list1: L1, 
	       mbit-cnt: N, 
	       atbound-cnt: N', 
	       mtbound-cnt: N'', 
	       AS >
    { tG | SL }
    (v <- initialize)
    => rounds(xR)
       <name: v |
	         status: initialized,
	         round: xR,
	      	 list0: genRandBList(xR),
	      	 list1: genRandBList(xR), 
	         mbit-cnt: 0, 
	         atbound-cnt: 0, 
	         mtbound-cnt: 0, 
	         AS >
       { tG | SL }
       (v <- forwardHash) .

  --- Forward generated lists to prover
  rl [ForwardHash] : 
    <name: v |
	       status: initialized,
	       list0: L0,
	       list1: L1, 
	       AS >
    { tG | SL }
    (v <- forwardHash)
    => <name: v |
            status: ready,
	       list0: L0,
	       list1: L1, 
            AS >
         { tG | SL }
	   (p <- recordHash(L0,L1)) .

  --- Record received lists at the prover
  rl [RecordHash] : 
    <name: p |
        status: pending,
	    list0: L0,
	    list1: L1, 
        AS >
    { tG | SL }
    (p <- recordHash(L0',L1'))
    => <name: p |
            status: ready,
	    	list0: L0',
	   		list1: L1', 
            AS >
       { tG | SL }
       (v <- beginRound) .


  ***** Phase II
  ---- Begin an authentication round 
  ---- (samples X, and randomly selects the challenge)
  rl [BeginRound] :
    rounds(xR)
    <name: v |
	       status: ready,
	       round: iR,
         atime-sent: tS  ,
         mtime-sent: tS' ,
         atime-recv: tR  ,
         mtime-recv: tR' ,
	       challenge: cB,
	       AS >  
    { tG | SL }
    (v <- beginRound)
    => rounds(p(xR))
	     <name: v |
	         status: sending,
	         round: p(xR),
           atime-sent: (floor(tG) + 1.0 + sampleX) ,
           mtime-sent: 0.0 ,
           atime-recv: 0.0 ,
           mtime-recv: 0.0 ,
           challenge: b(if sampleBerWithP(0.5) then 0 else 1 fi) ,
	         AS >
	     mytick(insert( { tG | SL }, 
	         [ floor(tG) + 1.0, (v <- sendChallenge) , 0] )) .

  --- sending out a challenge
  rl [Challenge] :
    h(fH)
    <name: v |
        status: sending,
	      list0: (B0 L0),
	      list1: (B1 L1),
        atime-sent: tS,
        challenge: cB, AS >
    { tG | SL }
    (v <- sendChallenge)
    => h(fH) 
      <name: v |
          status: sent,
	      	list0: (L0 B0),
	      	list1: (L1 B1), 
          atime-sent: tS, 
          challenge: cB, AS >
       mytick(insert(insert( { tG | SL } ,
              [ floor(tG) + 1.0, (v <- recordTime), 0]),  
              [ tS + xDelay(fH), (p <- challenge(cB)), 0])) .

  --- Record the (measured) time at which the challenge was sent (samples Y)
  rl [RecordTimeSent] :
    <name: v |
        status: sent,
        atime-sent: tS,
        mtime-sent: tS', AS >
    { tG | SL }
    (v <- recordTime)
    => <name: v |
            status: receiving,
            atime-sent: tS,
            mtime-sent: (tG + sampleY),  AS >
       mytick({ tG | SL }) .

  --- Respond to a challenge
  rl [Respond] :
    rounds(iR)
    h(fH)
    <name: p |
	       status: ready,
	       list0: (B0 L0),
	       list1: (B1 L1), 
	       AS >
    { tG | SL }
    (p <- challenge(cB))
    => rounds(iR)
       h(fH)
       <name: p |
	         status: ready,
	       	 list0: (L0 B0),
	       	 list1: (L1 B1), 
	         AS >
       mytick(insertList( { tG | SL } ,  
	   	---- A one-round session or multi-round with no guess-ahead
    	if ROUNDS == 1 or GAHEAD == false then 
	    	[ tG + xDelay(fH), (v <- response(detResp(NOISE, cB, PTYPE, B0, B1))), 0]  
	   	---- The first round in a multi-round sesssion (with guess-ahead)
	  	else if iR == ROUNDS - 1 then   
	      [ tG + xDelay(fH), (v <- response(detResp(NOISE, cB, PTYPE, B0, B1))), 0] ; 
        [ tG + 3.0 * xDelay(fH) - gATD(fH), (v <- response(gssResp(NOISE, PTYPE, head(L0), head(L1)))), 0]
	   	---- Any round other than the last round (with guess-ahead)
	  	else if iR > 0 then    
        [ tG + 3.0 * xDelay(fH) - gATD(fH), (v <- response(gssResp(NOISE, PTYPE, head(L0), head(L1)))), 0]
      ---- The last round 
	  	else 
			nil 
	  	fi fi fi )) .	   

  op detResp : Float Bit Nat Bit Bit -> MaybeBit .
  eq detResp(fB, cB, iG, B0, B1) 
    = if not(sampleBerWithP(fB)) then 
        detBit(cB, iG, B0, B1)
      else
        bN
      fi .

  op gssResp : Float Nat Bit Bit -> MaybeBit .
  eq gssResp(fB, iG, B0, B1) 
    = if not(sampleBerWithP(fB)) then 
        gssBit(iG, B0, B1)
      else
        bN
      fi .

	op detBit : Bit Nat Bit Bit -> Bit .
	eq detBit(cB, iG, B0, B1) 
	   = if iG == 0 then    --- kProver
	     	if cB == b(0) then B0 else B1 fi
	     else    --- gProver
	        gssBit(iG, B0, B1) 
	     fi .

	op gssBit : Nat Bit Bit -> Bit .
	eq gssBit(iG, B0, B1) 
	   = if iG == 1 then    --- gProver1
	        if sampleBerWithP(0.5) then B0 else B1 fi
	     else    ---gProver2 
	        b( if sampleBerWithP(0.5) then 0 else 1 fi )
	     fi .


---(
    --- Ignore any extra challenge at the end
    rl [Respond] :
        rounds(0)
        <name: p | status: ready, AS >
        { tG | SL }
        (p <- challenge(N))
      => rounds(0)
         <name: p | status: ready, AS >
         { tG | SL } .
)
    

  ---- Receive a response (samples Z)
  rl [Response] :
    <name: v |
        status: receiving,
        atime-recv: tS, 
	      response: cP, AS >
    { tG | SL }
    (v <- response(cP'))
    => <name: v |
            status: received,
            atime-recv: tG, 
  	        response: cP', AS >
       mytick(insert( { tG | SL } ,
              [ floor(tG) + 1.0 + sampleZ , (v <- recordTime), 0])) .


    --- Abort the protocol if a response is received too soon!
  crl [Abort] :
    rounds(iR)
    <name: v |
        status: ST,
        response: cP, AS >
    { tG | SL }
    (v <- response(cP'))
    => rounds(0)
	     <name: v |
            status: aborted,
            response: cP', AS >
       { tG | SL } 
    if ST =/= receiving .          

   
  ---- Record the time, verify the response and record the result
  crl [Verify1] : 
    <name: v |
        status: received,
	       list0: (L0 B0),
	       list1: (L1 B1), 
        atime-sent:  tS',
        atime-recv:  tR',
        mtime-sent:  tS,
        mtime-recv:  tR,
        challenge:   cB,
        response:    cP, 
        mbit-cnt:    N, 
        atbound-cnt: N', 
        mtbound-cnt: N'', 
        AS >
    { tG | SL }
    (v <- recordTime)
    => <name: v |
            status: ready,
	       list0: (L0 B0),
	       list1: (L1 B1), 
            atime-sent:  tS',
            atime-recv:  tR',
            mtime-sent:  tS,
            mtime-recv:  tG,
            challenge:   cB,
            response:    cP, 
            mbit-cnt:    (if mBit? then s(N) else N fi ), 
            atbound-cnt: (if atBound? then s(N') else N' fi ), 
            mtbound-cnt: (if mtBound? then s(N'') else N'' fi ), 
            AS >
       mytick(insert( { tG | SL } ,
              [ floor(tG) + 1.0, (v <- beginRound), 0]))
    if mBit?  := notNoise(cP) and ((cB == b(0) and cP == B0) or (cB == b(1) and cP == B1))
	/\ atBound? := notNoise(cP) and tR' - tS' <= MAXRTT 
	/\ mtBound? := notNoise(cP) and tG - tS <= MAXRTT .

endm


mod DBP-ANALYSIS is
    pr CONVERSION .
    inc DBP-BEHAVIOR .

    vars N N' iR : Nat .
    var  C : Config .
    var  AS : AttributeSet .
    vars tRTT tS tR fH : Float .
    var St : Status .

    *******************************************************************
    ***** 0. Acceptance based on the *measured* RTT being within the bound

    op acceptedMT : Config -> Float .
    eq acceptedMT(
          <name: v | 
  	       status: St ,
               mtbound-cnt: N,
               AS > 
	  C )
      = if float(N) >= MIN-MTR and St == ready then 1.0 else 0.0 fi .

    *********************************************************************
    ***** 1. Rejection based on the *measured* RTT being more than the bound

    op rejectedMT : Config -> Float .
    eq rejectedMT(
          <name: v | 
  	       status: St ,
               mtbound-cnt: N,
               AS > 
	  C )
      = if float(N) < MIN-MTR or St == aborted then 1.0 else 0.0 fi .

    
    *******************************************************************
    ***** 2. Acceptance based on the *actual* RTT being within the bound

    op acceptedAT : Config -> Float .
    eq acceptedAT(
          <name: v |
               status: St ,
               atbound-cnt: N,
               AS >
          C )
      = if float(N) >= MIN-ATR and St == ready then 1.0 else 0.0 fi .

    *********************************************************************
    ***** 3. Rejection based on the *actual* RTT being more than the bound

    op rejectedAT : Config -> Float .
    eq rejectedAT(
          <name: v |
               status: St ,
               atbound-cnt: N,
               AS >
          C )
      = if float(N) < MIN-ATR or St == aborted then 1.0 else 0.0 fi .


    **************************************************************
    ***** 4. Acceptance based on the number of matching bits received

    op acceptedMB : Config -> Float .
    eq acceptedMB(
	  <name: v | status: St ,
	             mbit-cnt: N,
	             AS > C )
      = if float(N) >= MIN-MBR and St == ready then 1.0 else 0.0 fi .

    *****************************************************************
    ***** 5. Rejection based on the number of non-matching bits received

    op rejectedMB : Config -> Float .
    eq rejectedMB(
	  <name: v | status: St ,
	             mbit-cnt: N,
	             AS > C )
      = if float(N) < MIN-MBR or St == aborted then 1.0 else 0.0 fi .


    *****************************************************************
    ***** Other functions

    ---- 6. MT Success rate
    op sRateMT : Config -> Float .
    eq sRateMT(
	  <name: v | status: ready ,
	             mtbound-cnt: N,
	             AS > C )
      = float(N) / float(ROUNDS) .

    ---- 7. AT Success rate
    op sRateAT : Config -> Float .
    eq sRateAT(
	  <name: v | status: ready ,
	             atbound-cnt: N,
	             AS > C )
      = float(N) / float(ROUNDS) .

    ---- 8. MB Success rate
    op sRateMB : Config -> Float .
    eq sRateMB(
	  <name: v | status: ready ,
	             mbit-cnt: N,
	             AS > C )
      = float(N) / float(ROUNDS) .


    *****************************************************************
    ***** Compound functions

    ---- 9. Accepted by both MT and MB
    op acceptedMTMB : Config -> Float .
    eq acceptedMTMB(C) 
	= if acceptedMT(C) > 0.0 and acceptedMB(C) > 0.0 then 1.0 else 0.0 fi .

    ---- 10. Rejected by either MT or MB
    op rejectedMTMB : Config -> Float .
    eq rejectedMTMB(C) 
	= if rejectedMT(C) > 0.0 or rejectedMB(C) > 0.0 then 1.0 else 0.0 fi .

    ---- 11. Accepted by both AT and MB
    op acceptedATMB : Config -> Float .
    eq acceptedATMB(C) 
	= if acceptedAT(C) > 0.0 and acceptedMB(C) > 0.0 then 1.0 else 0.0 fi .

    ---- 12. Rejected by either AT or MB
    op rejectedATMB : Config -> Float .
    eq rejectedATMB(C) 
	= if rejectedAT(C) > 0.0 or rejectedMB(C) > 0.0 then 1.0 else 0.0 fi .

    ---- 13. Attack detected
    op attDetected : Config -> Float .
    eq attDetected(<name: v | status: St , AS > C )
      = if St == aborted then 1.0 else 0.0 fi .



    ***** to be used by PVeStA
    eq tick(rounds(0) h(fH) C) =  rounds(ROUNDS) h(sampleRD) C .

    ----eq sat(0, C) = complete(C) .

    ---- measured time
    eq val(0, C) = acceptedMT(C) .
    eq val(1, C) = rejectedMT(C) .
    ---- actual time
    eq val(2, C) = acceptedAT(C) .
    eq val(3, C) = rejectedAT(C) .
    ---- matching bits
    eq val(4, C) = acceptedMB(C) .
    eq val(5, C) = rejectedMB(C) .
    ---- success rate
    eq val(6, C) = sRateMT(C) .
    eq val(7, C) = sRateAT(C) .
    eq val(8, C) = sRateMB(C) .
    ---- compound functions
    eq val(9, C) = acceptedMTMB(C) .
    eq val(10,C) = rejectedMTMB(C) .
    eq val(11,C) = acceptedATMB(C) .
    eq val(12,C) = rejectedATMB(C) .
    ---- 
    eq val(13,C) = attDetected(C) .
 

endm

mod MODEL-PARAMS is
  pr SAMPLER . 
  inc PARAMS .

  var tX fH fX : Float .

  --- Note: 
  ---    0 <= rand  <= 1  (Float)
  ---    0 <= rrand <= 1  (rational)
  ---    0 <  prand <= 1  (Float)

  --- Some useful quantities
  --------------------------
  ops rand2 rand4 : -> [Float] .
  eq  rand2 = rand / 2.0 .
  eq  rand4 = rand / 4.0 .

  ops prand2 prand4 psrand2 psrand4 : -> [Float] .
  eq  prand2  = prand / 2.0 .      --- 0.0  < prand2  <= 0.5
  eq  prand4  = prand / 4.0 .      --- 0.0  < prand4  <= 0.25
  eq  psrand2 = prand2 + 0.5 .    ---  0.5  < psrand2 <= 1.0
  eq  psrand4 = prand4 + 0.25 .    --- 0.25 < psrand4 <= 0.5
    
  ops all smr lmr : -> [Float] .
  eq smr = float(ROUNDS) / 2.0 .
  eq lmr = float(ROUNDS) * 2.0 / 3.0 .
  eq all = float(ROUNDS) .

  op rttm : Float -> [Float] .
  eq rttm(tX) = if MAXRTT > tX then MAXRTT - tX else 0.0 fi .

  op rttt : Float -> [Float] .
  eq rttt(tX) = MAXRTT * tX .

  op range : Float Float -> Float .
  eq range(fH, fX) = genRandom(- fH / fX, fH / fX) .

  --- guess ahead strategies
  ops cn cn0 ag : -> [Float] .
  ops pr cn ag  : Float -> [Float] .

  eq pr(fH) = 2.0 * fH .
  eq cn	    = genRandom(0.5, 2.0 * 0.5) .
  eq cn0    = genRandom(0.25, 2.0 * 0.25) .
  eq ag	    = genRandom(1.5 * 0.5, 2.5 * 0.5) . 
  eq cn(fH) = genRandom(fH, 2.0 * fH) .
  eq ag(fH) = genRandom(1.5 * fH, 2.5 * fH) . 

  --- Model parameters
  --------------------
  eq ROUNDS = 50 .
  eq MAXRTT = 4.0 .
  eq NOISE = 0.05 .

  eq VDCLK = false .
  eq sampleX = rand2 .
  eq sampleY = rand2 .
  eq sampleZ = rand2 .

  eq PTYPE = 1 .
  eq sampleRD = prand2 .
  eq GAHEAD = true .
  eq gATD(fH) = ag .

  --- Acceptance threshold levels
  eq MIN-MTR = all .
  eq MIN-ATR = lmr .
  eq MIN-MBR = lmr .

  --- (computed -- shouldn't be modified directly)
  --- Transmission delay: (R + H) / 2 
  eq xDelay(fH) = (MAXRTT + fH) / 2.0 .
  --- Additional delay for guess-ahead attacks 
  eq VDCDELAY = 0.0 . ---- if VDCLK then 1.0 else 0.0 fi .

endm

mod DBP is
  inc MODEL-PARAMS .
  inc DBP-ANALYSIS .

  --- Initial State  
  eq initState =
      rounds(0)
      h(0.0)
    	<name: v |
          status:      pending,
          round:       0,
 	        list0:       nilBL,
 	        list1:       nilBL,
          atime-sent:  0.0 ,
          mtime-sent:  0.0 ,
          atime-recv:  0.0 ,
          mtime-recv:  0.0 ,
          challenge:   b(0),
          response:    bN, 
          mbit-cnt:    0,
          atbound-cnt: 0,
          mtbound-cnt: 0
      >
      <name: p |
          status: pending,
 	        list0:       nilBL,
 	        list1:       nilBL
      >
      { 0.0 | nil }
      (v <- initialize) .    
 
endm
        
